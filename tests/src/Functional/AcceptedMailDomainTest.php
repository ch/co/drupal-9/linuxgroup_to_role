<?php

namespace Drupal\Tests\linuxgroup_to_role\Functional;

use Drupal\linuxgroup_to_role\UserInfo;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests linuxgroup_to_role functionality.
 *
 * @group linuxgroup_to_role
 */
class AcceptedMailDomainTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['linuxgroup_to_role_testdata'];

  /**
   * Theme to use for tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Mock our user_info service.
   *
   * @var \Drupal\linuxgroup_to_role\UserInfoInterface
   */
  protected $userInfoMock;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $configFactory = $this->container->get('config.factory');
    $loggerFactory = $this->container->get('logger.factory');

    // Create a UserInfo object where we mock the getpwnam call.
    $this->userInfoMock = $this->getMockBuilder(UserInfo::class)
      ->setConstructorArgs([$configFactory, $loggerFactory])
      ->setMethods(['getpwnam'])
      ->getMock();

    // Mock getpwnam. We should return a data structure compatible
    // with posix_getpwnam(), though we only make use of the uid
    // attribute in the UserInfo class.
    $this->userInfoMock->expects($this->any())
      ->method('getpwnam')
      ->willReturnCallback(function (string $username) {
        if ($username == 'localuser') {
          return ['uid' => 1000];
        }
        elseif ($username == 'externaluser') {
          return ['uid' => 1001];
        }
        else {
          return FALSE;
        }
      });

  }

  /**
   * Test data for testUsernameWithMailDomain.
   */
  protected function usernameUidExampleData() {
    yield 'external user with accepted domain' => ['externaluser@example.com', 1001];
    yield 'external user with non-accepted domain' => ['externaluser@baddomain.com', FALSE];
    yield 'local user' => ['localuser', 1000];
  }

  /**
   * Test handling of usernames with mail domains.
   *
   * @dataProvider usernameUidExampleData()
   */
  public function testUsernameWithMailDomain(string $username, int|false $expected) {

    $user = $this->drupalCreateUser([], $username);

    $uid = $this->userInfoMock->getUid($user);

    $this->assertEquals($expected, $uid);
  }

}
