<?php

namespace Drupal\Tests\linuxgroup_to_role\Functional;

use Drupal\linuxgroup_to_role\AuthorizationManager;
use Drupal\linuxgroup_to_role\UserInfoInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests linuxgroup_to_role functionality.
 *
 * @group linuxgroup_to_role
 */
class RoleAssignmentTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['linuxgroup_to_role_testdata'];

  /**
   * Theme to use for tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Mock our user_info service.
   *
   * @var \Drupal\linuxgroup_to_role\UserInfoInterface
   */
  protected $userInfoMock;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create a mock for the UserInfoInterface.
    $this->userInfoMock = $this->createMock(UserInfoInterface::class);
    $this->userInfoMock->expects($this->any())
      ->method('getUid')
      ->willReturn(1001);
  }

  /**
   * List of groups that does include group1.
   */
  public function groupList1() {
    yield 'user is in group1 only' => [['group1']];
    yield 'user is in group1 and group2' => [['group1', 'group2']];
  }

  /**
   * List of groups that do not include group1.
   */
  public function groupList2() {
    yield 'user is in no groups' => [[]];
    yield 'user is in group3' => [['group3']];

  }

  /**
   * List of groups that have a case-dependent mapping.
   */
  public function groupList3() {
    yield 'user is in group grouptwo' => [['grouptwo']];
  }

  /**
   * Test that role is assigned when user is in the mapped group.
   *
   * @dataProvider groupList1()
   */
  public function testAssignmentOfMappedRole($groups) {
    $this->userInfoMock->expects($this->any())
      ->method('getGroups')
      ->willReturn($groups);

    // Create a user.
    $user = $this->drupalCreateUser();

    $authorizationManager = new AuthorizationManager(
      $this->container->get('entity_type.manager'),
      $this->container->get('config.factory'),
      $this->container->get('logger.factory'),
      $this->userInfoMock
    );

    $this->assertFalse($user->hasRole('group1_role'), 'group1_role absent before processing');
    $authorizationManager->processUser($user);
    $this->assertTrue($user->hasRole('group1_role'), 'group1_role granted after processing');

  }

  /**
   * Test that role is not assigned when user is not in the mapped group.
   *
   * @dataProvider groupList2()
   */
  public function testNonAssignmentOfMappedRole($groups) {

    $this->userInfoMock->expects($this->any())
      ->method('getGroups')
      ->willReturn($groups);

    // Create a user.
    $user = $this->drupalCreateUser();

    $authorizationManager = new AuthorizationManager(
      $this->container->get('entity_type.manager'),
      $this->container->get('config.factory'),
      $this->container->get('logger.factory'),
      $this->userInfoMock
    );

    $this->assertFalse($user->hasRole('group1_role'), 'group1_role role absent before processing');
    $authorizationManager->processUser($user);
    $this->assertFalse($user->hasRole('group1_role'), 'group1_role role absent after processing');

  }

  /**
   * Test removal of role when user is not in group.
   *
   * @dataProvider groupList2()
   */
  public function testRemovalOfMappedRole($groups) {

    $this->userInfoMock->expects($this->any())
      ->method('getGroups')
      ->willReturn($groups);

    // Create a user.
    $user = $this->drupalCreateUser();

    $authorizationManager = new AuthorizationManager(
      $this->container->get('entity_type.manager'),
      $this->container->get('config.factory'),
      $this->container->get('logger.factory'),
      $this->userInfoMock
    );

    $user->addRole('group1_role');

    $this->assertTrue($user->hasRole('group1_role'), 'group1_role role present before processing');
    $authorizationManager->processUser($user);
    $this->assertFalse($user->hasRole('group1_role'), 'group1_role role absent after processing');

  }

  /**
   * Test manually-added unmapped roles persist after processing.
   *
   * By default, the module does not remove roles from users if
   * the role concerned does not have a configured mapping.
   *
   * @dataProvider groupList2()
   */
  public function testPermanenceOfUnmappedRole($groups) {

    $this->userInfoMock->expects($this->any())
      ->method('getGroups')
      ->willReturn($groups);

    // Create a user.
    $user = $this->drupalCreateUser();

    $authorizationManager = new AuthorizationManager(
      $this->container->get('entity_type.manager'),
      $this->container->get('config.factory'),
      $this->container->get('logger.factory'),
      $this->userInfoMock
    );

    $user->addRole('member');

    $this->assertTrue($user->hasRole('member'), 'member role present before processing');
    $authorizationManager->processUser($user);
    $this->assertTrue($user->hasRole('member'), 'member role present after processing');
  }

  /**
   * Test manually-added unmapped roles removed after processing.
   *
   * The module should remove manually-added but unmapped roles
   * if remove_unmanaged_roles is set.
   *
   * @dataProvider groupList2()
   */
  public function testRemovalOfUnmappedRole($groups) {

    $this->userInfoMock->expects($this->any())
      ->method('getGroups')
      ->willReturn($groups);

    // Create a user.
    $user = $this->drupalCreateUser();

    $configFactory = $this->container->get('config.factory');
    $configFactory->getEditable('linuxgroup_to_role.settings')->set('remove_unmanaged_roles', TRUE)->save();

    $authorizationManager = new AuthorizationManager(
      $this->container->get('entity_type.manager'),
      $configFactory,
      $this->container->get('logger.factory'),
      $this->userInfoMock
    );

    $user->addRole('member');

    $this->assertTrue($user->hasRole('member'), 'member role present before processing');
    $authorizationManager->processUser($user);
    $this->assertFalse($user->hasRole('member'), 'member role absent after processing');
  }

  /**
   * Test mapping where group mapping differs in case from user membership.
   *
   * @dataProvider groupList3()
   */
  public function testCaseSensitiveMapping($groups) {

    $this->userInfoMock->expects($this->any())
      ->method('getGroups')
      ->willReturn($groups);

    // Create a user.
    $user = $this->drupalCreateUser();

    $authorizationManager = new AuthorizationManager(
      $this->container->get('entity_type.manager'),
      $this->container->get('config.factory'),
      $this->container->get('logger.factory'),
      $this->userInfoMock
    );

    // Mappings assume group names are case sensitive by default, so
    // no role should be assigned.
    $this->assertFalse($user->hasRole('group2_role'), 'group2_role absent before case-sensitive processing');
    $authorizationManager->processUser($user);
    $this->assertFalse($user->hasRole('group2_role'), 'group2_role absent after case-sensitive processing');

    // Now turn on case insensitivity.
    $configFactory = $this->container->get('config.factory');
    $configFactory->getEditable('linuxgroup_to_role.settings')->set('case_insensitive_group_matching', TRUE)->save();

    $authorizationManager = new AuthorizationManager(
      $this->container->get('entity_type.manager'),
      $this->container->get('config.factory'),
      $this->container->get('logger.factory'),
      $this->userInfoMock
    );

    $authorizationManager->processUser($user);
    $this->assertTrue($user->hasRole('group2_role'), 'group2_role present after case-insensitive processing');

  }

}
