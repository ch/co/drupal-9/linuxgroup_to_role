<?php

namespace Drupal\Tests\linuxgroup_to_role\Functional;

use Drupal\linuxgroup_to_role\AuthorizationManager;
use Drupal\linuxgroup_to_role\UserInfoInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests linuxgroup_to_role functionality.
 *
 * @group linuxgroup_to_role
 */
class NoLocalUserTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['linuxgroup_to_role_testdata'];

  /**
   * Theme to use for tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Mock our user_info service.
   *
   * @var \Drupal\linuxgroup_to_role\UserInfoInterface
   */
  protected $userInfoMock;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create a mock for the UserInfoInterface.
    $this->userInfoMock = $this->createMock(UserInfoInterface::class);
    $this->userInfoMock->expects($this->any())
      ->method('getUid')
      ->willReturn(FALSE);

    // When we have no uid, we should never call getGroups.
    $this->userInfoMock->expects($this->exactly(0))
      ->method('getGroups')
      ->willReturn([]);
  }

  /**
   * Test that nothing is done when no local user is found.
   */
  public function testNoActionWhenUserIsNotLocal() {

    // Create a user.
    $user = $this->drupalCreateUser();

    $user->addRole('editor');

    $authorizationManager = new AuthorizationManager(
    $this->container->get('entity_type.manager'),
    $this->container->get('config.factory'),
    $this->container->get('logger.factory'),
    $this->userInfoMock
    );

    $this->assertFalse($user->hasRole('administrator'), 'administrator role absent before processing');
    $this->assertTrue($user->hasRole('editor'), 'editor role present before processing');
    $authorizationManager->processUser($user);
    $this->assertFalse($user->hasRole('administrator'), 'administrator role absent after processing');
    $this->assertTrue($user->hasRole('editor'), 'editor role present after processing');
  }

}
