<?php

namespace Drupal\linuxgroup_to_role;

use Drupal\user\UserInterface;

/**
 * Provide linuxgroup_to_role.user_info service.
 */
interface UserInfoInterface {

  /**
   * Get local uid for a Drupal user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user account of interest.
   *
   * @return int
   *   The numeric UID if found, false otherwise.
   */
  public function getUid(UserInterface $user) : int|false;

  /**
   * Get a list of group names for a user.
   *
   * @param int $uid
   *   Local user ID of interest.
   *
   * @return mixed
   *   Group names that the processed user is a member of.
   */
  public function getGroups(int $uid);

  /**
   * Return an array of info for username.
   *
   * @param string $username
   *   An alphanumeric username.
   *
   * @return array|false
   *   On success, an array of info about the user, false othewise
   */
  public function getpwnam(string $username): array|false;

}
