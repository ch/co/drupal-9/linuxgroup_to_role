<?php

namespace Drupal\linuxgroup_to_role\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush commands for managing group/role mappings.
 */
class LinuxGroupToRoleCommands extends DrushCommands {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Show the current mapping.
   *
   * @command linuxgroup_to_role:showmap
   * @aliases lgtr-showmap
   */
  public function showMap() {
    $settings = $this->configFactory->get('linuxgroup_to_role.settings');
    $role_mappings = $settings->get('role_mappings');

    if (!$role_mappings) {
      $this->logger()->log('notice', 'No mappings have yet been defined');
      return;
    }

    $msg = "Linux group to role mapping:\n";
    foreach ($role_mappings as $uuid => $mapping) {
      $group_name = $mapping['group_name'];
      $role_name = $mapping['role_name'];

      // Append the mapping information to the message.
      $msg .= "$group_name: $role_name\n";
    }
    $this->logger()->log('notice', $msg);
  }

  /**
   * Add a group to role mapping.
   *
   * @param string $group
   *   Group name to map from.
   * @param string $role
   *   Role name to map to.
   *
   * @command linuxgroup_to_role:addmapping
   * @aliases lgtr-addmap
   *
   * @usage drush lgtr-addmap groupname rolename
   */
  public function addMapping($group, $role) {

    if ($this->checkMappingExists($group, $role)) {
      $this->logger()->notice("Group $group is already mapped to $role");
      return;
    }

    $valid_roles = $this->getValidRoles();

    if (!in_array($role, $valid_roles)) {
      $roles = implode(", ", $valid_roles);
      $this->logger()->error("Role $role cannot be mapped to. Valid roles are: $roles");
      return;
    }

    $settings = $this->configFactory->getEditable('linuxgroup_to_role.settings');

    // Get the existing role mappings.
    $map = $settings->get('role_mappings') ?? [];

    // Generate a UUID for the new mapping.
    $uuid = \Drupal::service('uuid')->generate();

    // Add the new mapping with group_name and role_name.
    $map[$uuid] = [
      'group_name' => $group,
      'role_name' => $role,
    ];

    // Save the updated mapping.
    $settings->set('role_mappings', $map)->save();

    $this->logger()->notice("Added mapping from group $group to role $role");
  }

  /**
   * Remove a mapping from group to role.
   *
   * @param string $group
   *   Group name.
   * @param string $role
   *   Role name.
   *
   * @command linuxgroup_to_role:removemapping
   * @aliases lgtr-removemap lgtr-delmap
   *
   * @usage drush lgtr-delmap groupname rolename
   */
  public function removeMapping($group, $role) {

    if (!$this->checkMappingExists($group, $role)) {
      $this->logger()->log('error', "Group $group is not mapped to role $role");
      return;
    }

    $settings = $this->configFactory->getEditable('linuxgroup_to_role.settings');
    $map = $settings->get('role_mappings');

    $uuid_to_remove = NULL;
    foreach ($map as $uuid => $mapping) {
      if ($mapping['group_name'] === $group && $mapping['role_name'] === $role) {
        $uuid_to_remove = $uuid;
        break;
      }
    }

    if (!$uuid_to_remove) {
      $this->logger()->error("Unable to determine uuid for requested mapping");
      return;
    }

    unset($map[$uuid_to_remove]);

    $settings->set('role_mappings', $map)->save();
    $this->logger()->notice("Removed mapping from $group to $role");
  }

  /**
   * Show list of accepted mail domains.
   *
   * @command linuxgroup_to_role:showdomains
   * @aliases lgtr-showdom
   */
  public function showDomains() {
    $settings = $this->configFactory->get('linuxgroup_to_role.settings');
    $domains = $settings->get('accepted_mail_domains');

    if (!$domains) {
      $this->logger()->log('notice', "No mail domains have been configured");
      return;
    }

    $msg = "The following email domains will be removed from usernames when looking up local group membership: \n";
    $msg .= implode("\n", $domains);

    $this->logger()->log('notice', $msg);
  }

  /**
   * Add a domain.
   *
   * @param string $domain
   *   The domain to add.
   *
   * @command linuxgroup_to_role:adddomain
   * @aliases lgtr-adddom
   */
  public function addDomain($domain) {
    $settings = $this->configFactory->getEditable('linuxgroup_to_role.settings');
    $domains = $settings->get('accepted_mail_domains');

    if ($domains && in_array($domain, $domains)) {
      $this->logger()->log('notice', "$domain is already in the list of domains");
      return;
    }

    $domains[] = $domain;

    $settings->set('accepted_mail_domains', $domains)->save();
    $this->logger()->log('notice', "$domain has been added to the list of domains");
  }

  /**
   * Remove a domain.
   *
   * @param string $domain
   *   The domain to remove.
   *
   * @command linuxgroup_to_role:deldomain
   * @aliases lgtr-deldom
   */
  public function removeDomain($domain) {
    $settings = $this->configFactory->getEditable('linuxgroup_to_role.settings');
    $domains = $settings->get('accepted_mail_domains');

    if (!in_array($domain, $domains)) {
      $this->logger()->log('notice', "$domain is not in the list of domains");
      return;
    }

    foreach ($domains as $k => $v) {
      if ($v === $domain) {
        unset($domains[$k]);
        break;
      }
    }

    $settings->set('accepted_mail_domains', $domains)->save();
    $this->logger()->log('notice', "$domain has been removed from the list of domains");

  }

  /**
   * Manage remove_unmanaged_roles setting.
   *
   * @param string $setting
   *   If false (default), do not remove roles from users if not mapped by this
   *   module.
   *
   * @command linuxgroup_to_role:remove_unmanaged_roles
   * @aliases lgtr-rur
   */
  public function setRemoveUnmanagedRoles(string $setting) {
    if ($setting !== "true"  && $setting !== "false") {
      $this->logger()->log('error', "Invalid option; specify either true or false");
      return;
    }

    $settings = $this->configFactory->getEditable('linuxgroup_to_role.settings');

    if ($setting === "false") {
      $boolsetting = FALSE;
    }
    else {
      $boolsetting = TRUE;
    }

    $settings->set('remove_unmanaged_roles', $boolsetting)->save();
  }

  /**
   * Manage case_insensitive_group_matching setting.
   *
   * @param string $setting
   *   If false (default), group name mappings are case-sensitive.
   *
   * @command linuxgroup_to_role:case_insensitive_group_matching
   * @aliases lgtr-cigm
   */
  public function setCaseInsensitiveGroupMatching(string $setting) {
    if ($setting !== "true"  && $setting !== "false") {
      $this->logger()->log('error', "Invalid option; specify either true or false");
      return;
    }

    $settings = $this->configFactory->getEditable('linuxgroup_to_role.settings');

    if ($setting === "false") {
      $boolsetting = FALSE;
    }
    else {
      $boolsetting = TRUE;
    }

    $settings->set('case_insensitive_group_matching', $boolsetting)->save();
  }

  /**
   * Check if a mapping entry already exists.
   *
   * @param string $group_name
   *   Group name.
   * @param string $role_name
   *   Role name.
   *
   * @return bool
   *   True if the mapping exists, false otherwise.
   */
  private function checkMappingExists($group_name, $role_name) {
    $settings = $this->configFactory->get('linuxgroup_to_role.settings');
    $map = $settings->get('role_mappings');

    if ($map) {
      foreach ($map as $mapping) {
        if ($mapping['group_name'] === $group_name && $mapping['role_name'] === $role_name) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Get a list of role names that can be mapped to.
   *
   * @return array
   *   Array of acceptable role names.
   */
  private function getValidRoles() {
    // A list of all roles excluding 'anonymous' ...
    $roles = array_keys(user_roles(TRUE));

    // ...from which we then also remove 'authenticated'
    // because we shouldn't try to assign a group to that
    // role
    foreach ($roles as $idx => $role) {
      if ($role == 'authenticated') {
        unset($roles[$idx]);
      }
    }

    return $roles;
  }

}
