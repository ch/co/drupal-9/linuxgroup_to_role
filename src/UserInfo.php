<?php

namespace Drupal\linuxgroup_to_role;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\user\UserInterface;

/**
 * Provide linuxgroup_to_role.user_info service.
 */
class UserInfo implements UserInfoInterface {
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Construct an instance of the service.
   */
  public function __construct(ConfigFactoryInterface $config_factory_manager,
      LoggerChannelFactoryInterface $logger_factory) {
    $this->configFactory = $config_factory_manager;
    $this->loggerFactory = $logger_factory->get('linuxgroup_to_role');
  }

  /**
   * {@inheritdoc}
   */
  public function getUid(UserInterface $user) : int|false {
    $username = $user->getAccountName();

    if (str_contains($username, '@')) {
      $parts = explode('@', $username);
      $local_part = $parts[0];
      $domain = $parts[1];

      $accepted_domains = $this->configFactory->get('linuxgroup_to_role.settings')->get('accepted_mail_domains');
      if ($accepted_domains && in_array($domain, $accepted_domains)) {
        $username = $local_part;
      }
    }

    $user_info = $this->getpwnam($username);

    if ($user_info) {
      return $user_info['uid'];
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getpwnam(string $username): array|false {
    return posix_getpwnam($username);
  }

  /**
   * {@inheritdoc}
   * */
  public function getGroups(int $uid) {
    $uid = escapeshellarg($uid);
    exec("/usr/bin/id -G $uid", $output, $rc);

    if ($rc != 0) {
      $this->loggerFactory->error("Error looking up groups for " . $this->user->getAccountName());
      return [];
    }

    $gids = explode(' ', $output[0]);

    $groups = [];

    foreach ($gids as $gid) {
      $group_info = posix_getgrgid($gid);
      $groups[] = $group_info['name'];
    }

    return $groups;
  }

}
