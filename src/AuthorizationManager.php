<?php

namespace Drupal\linuxgroup_to_role;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\user\UserInterface;

/**
 * Manager class for linuxgroup_to_role.manager service.
 */
class AuthorizationManager {
  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * User info service.
   *
   * @var \Drupal\linuxgroup_to_role\UserInfoInterface
   */
  protected $userInfo;

  /**
   * The user account to process.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Construct an instance of the service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
      ConfigFactoryInterface $config_factory_manager,
      LoggerChannelFactoryInterface $logger_factory,
      UserInfoInterface $user_info) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory_manager;
    $this->loggerFactory = $logger_factory->get('linuxgroup_to_role');
    $this->userInfo = $user_info;
  }

  /**
   * Process a user being logged in.
   *
   *  @param\Drupal\user\UserInterface $user
   *    The user object on which the operation was just performed.
   */
  public function processUser(UserInterface $user) {
    $this->user = $user;
    $this->loggerFactory->notice($user->getAccountName() . " being processed");

    $uid = $this->userInfo->getUid($user);
    if ($uid === FALSE) {
      return;
    }

    $users_groups = $this->userInfo->getGroups($uid);

    $role_mappings = $this->configFactory->get('linuxgroup_to_role.settings')->get('role_mappings');

    $case_insensitive_group_matching = $this->configFactory->get('linuxgroup_to_role.settings')->get('case_insensitive_group_matching', FALSE);

    // Filter the role mappings to get only those that match the wanted groups.
    $desired_roles = [];

    $user_groups_to_check = $case_insensitive_group_matching
      ? array_map('strtolower', $users_groups)
      : $users_groups;

    foreach ($role_mappings as $uuid => $mapping) {
      $mapped_group_name = $case_insensitive_group_matching
        ? strtolower($mapping['group_name'])
        : $mapping['group_name'];

      if (in_array($mapped_group_name, $user_groups_to_check)) {
        $desired_roles[] = $mapping['role_name'];
      }
    }

    // Get the user's current roles.
    $current_roles = $user->getRoles(TRUE);

    // Determine roles to add and remove.
    $roles_to_add = array_diff($desired_roles, $current_roles);
    $roles_to_remove = array_diff($current_roles, $desired_roles);

    // Check if we should remove unmanaged roles.
    $remove_unmanaged_roles = $this->configFactory->get('linuxgroup_to_role.settings')->get('remove_unmanaged_roles');

    if (!$remove_unmanaged_roles) {
      // By default, we only remove roles which have a mapping.
      $managed_roles = array_column($role_mappings, 'role_name');
      $roles_to_remove = array_intersect($roles_to_remove, $managed_roles);
    }

    foreach ($roles_to_add as $role) {
      $user->addRole($role);
    }

    foreach ($roles_to_remove as $role) {
      $user->removeRole($role);
    }

    if (count($roles_to_add) > 0 || count($roles_to_remove) > 0) {
      $log_message = "Changing role membership of " . $user->getAccountName() . ". ";

      if (count($roles_to_add) > 0) {
        $log_message .= 'Assigning: ' . implode(',', $roles_to_add) . '. ';
      }

      if (count($roles_to_remove) > 0) {
        $log_message .= 'Removing: ' . implode(',', $roles_to_remove) . '. ';
      }

      $this->loggerFactory->notice($log_message);

      $user->save();
    }

  }

}
