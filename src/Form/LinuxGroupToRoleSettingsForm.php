<?php

namespace Drupal\linuxgroup_to_role\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Display settings for linuxgroup_to_role.
 */
class LinuxGroupToRoleSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'linuxgroup_to_role_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('linuxgroup_to_role.settings');

    $form['intro'] = [
      '#markup' => 'See README at <a href="https://gitlab.developers.cam.ac.uk/ch/co/drupal-9/linuxgroup_to_role/">https://gitlab.developers.cam.ac.uk/ch/co/drupal-9/linuxgroup_to_role/</a> for instructions on changing these settings.',
    ];

    // Display role mappings.
    $role_mappings = $config->get('role_mappings') ?? [];

    $rows = [];

    foreach ($role_mappings as $mapping) {
      $rows[] = [
        'group_name' => $mapping['group_name'] ?? '',
        'role_name' => $mapping['role_name'] ?? '',
      ];
    }

    $form['role_mappings'] = [
      '#type' => 'table',
      '#header' => ['Group Name', 'Role Name'],
      '#rows' => $rows,
      '#empty' => 'No role mappings configured.',
    ];

    // Display accepted mail domains.
    $form['accepted_mail_domains'] = [
      '#type' => 'item',
      '#title' => 'Accepted Mail Domains',
      '#markup' => '<ul><li>' . implode('</li><li>', $config->get('accepted_mail_domains') ?? []) . '</li></ul>',
    ];

    // Boolean settings.
    foreach (['remove_unmanaged_roles', 'case_insensitive_group_matching'] as $key) {
      $form[$key] = [
        '#type' => 'item',
        '#title' => $key,
        '#markup' => $config->get($key) ? 'Enabled' : 'Disabled',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Stub implememtation; form is readonly.
  }

}
