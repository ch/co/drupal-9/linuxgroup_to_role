Description
===========

Manage Drupal role assignments based upon membership of local linux groups.

Drupal users who do not have a local uid will be ignored by this module
(specifically, Drupal users for whom PHP's posix_getpwnam() returns a uid are
managed; other Drupal users are ignored).

Drupal roles will be added to or removed from a user account each time they log
in. 

By default, the module will only remove roles from users if a) there is a
mapping defined for the role, and b) the user is not a member of the mapped
local group. Thus, if you have 'first_role' and 'second_role' but only define a
mapping to assign a local group to 'first_role', then membership of
'second_role' can be managed manually.

**Warning** this means that if you:

- create a role mapping
- a user affected by the mapping logs in and so is granted a role
- remove that role mapping

then the role might **not** be removed from the user if the role is not involved
in another mapping. You can either remove the role manually, or see "Remove
unmanaged roles" for the setting which changes this behaviour.

It is also possible to define a list of 'accepted mail domains'. If a Drupal
user has a username of the form localpart@example.com and example.com is in the
list of accepted mail domains, the module will use localpart rather than
localpart@example.com when looking up the local uid and group membership.

The motivation for providing this feature is that OAuth2 just provides an email
address as one of the claims, and openid_connect then defaults to mapping that
to the Drupal username. The module needs to somehow deduce the local username
on the system from that address, so this setting is useful when local policies
dictate that user with the (as claimed by OAuth2) email address xyz@example.com
is also the local user 'xyz'.

Configuration
=============

All configuration is performed by Drush. All Drupal `rolename`s need to be the
machine name of the Drupal role, which you can enumerate via:

`drush role:list --fields=rid,label`

and then see the `rid` value. Or, log in to the Drupal site, navigate to
`People` (admin bar at top of screen), then Roles, 'Edit' the role of interest
and note the (non-editable) `Machine name`.

Group-to-role mapping
---------------------
drush lgtr-showmap: show the current role mappings, if any.

drush lgtr-addmap groupname rolename: Map members of local group `groupname` to
Drupal role `rolename`.

drush lgtr-delmap groupname rolename: Remove mapping of local group `groupname`
to Drupal role `rolename`.

Case (in)sensitive group matching
---------------------------------
drush lgtr-cigm true/false .

By default, group names are checked in a case-sensitive manner (e.g. if a
mapping is created for 'Domain Users' but the underlying group name returned is
'domain users', the mapped role will not be assigned).

If group matching should be performed in a case-insensitive manner, set this to
true.

Accepted mail domains
---------------------
drush lgtr-showdom: show the list of accepted mail domains, if any.

drush lgtr-adddom domain: add `domain` to the list of accepted mail domains.

drush lgtr-deldom domain: remove `domain` from the list of accepted mail
domains.

Remove unmanaged roles
----------------------
drush lgtr-rur true/false .

Defaults to false: roles which are not mapped to any group (i.e. "unmanaged")
will not be removed when a user logs in. Set this to true if you want such
roles to be removed from users.
